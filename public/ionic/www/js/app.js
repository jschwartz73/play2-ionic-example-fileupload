// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.services', 'ngCordova'])

.config(function ($compileProvider) {
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
})

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.controller('MainCtrl', function ($scope, Camera, $cordovaFileTransfer) {

    $scope.doUpload = function () {

        var server = "http://192.168.1.118:9000/uploads/rowid-123/3";

        alert($scope.lastPhoto);
        var options = {
            fileKey: "avatar",
            fileName: "3.jpg",
            chunkedMode: false,
            mimeType: "image/jpeg"
        };
        var trustAllHosts = true;

        $cordovaFileTransfer.upload(server, $scope.lastPhoto, options, trustAllHosts)
        .then(function(result) {
                alert('doUpload.success');
            // Success!
        }, function(err) {
                alert(JSON.stringify(err));
            // Error
        }, function (progress) {
            // constant progress updates
        });
    };

    $scope.getPhoto = function () {
        Camera.getPicture().then(function (imageURI) {
            console.log(imageURI);
            $scope.lastPhoto = imageURI;
            $scope.data.uploadurl = "http://localhost:9000/uploads/row239/3"
        }, function (err) {
            console.err(err);
        }, {
            quality: 75,
            targetWidth: 320,
            targetHeight: 320,
            saveToPhotoAlbum: false
        });
    };

})