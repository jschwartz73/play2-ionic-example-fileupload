package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

import java.io.File;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result upload(String rowId, String fileNumber) {

        Logger.info("Found file: " + rowId + ", fileNumber: " + fileNumber );

        File file = request().body().asMultipartFormData().getFile("avatar").getFile();
        Logger.info("File.size: " + file.length());

        return ok("File uploaded");
    }
}
